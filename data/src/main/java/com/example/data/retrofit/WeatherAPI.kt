package com.example.data.retrofit

import com.example.domain.models.Response
import retrofit2.Call
import retrofit2.http.GET

interface WeatherAPI {

    @GET("month?q=San%20Francisco")
    suspend fun getClimateFor30Days(): Response

}