package com.example.data.repository

import com.example.data.retrofit.WeatherAPI
import com.example.domain.models.Response
import com.example.domain.repository.Repository
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    private val weatherApi: WeatherAPI
): Repository {

    override suspend fun get30DaysWeather(): Response = weatherApi.getClimateFor30Days()
}