package com.reem.weatherapp.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.reem.weatherapp.databinding.TodaysWeatherListItemBinding


class TodayAdapter(val temperatures: String) :
    RecyclerView.Adapter<TodayViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodayViewHolder {
        val binding = TodaysWeatherListItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return TodayViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TodayViewHolder, position: Int) {
        //
    }

    override fun getItemCount(): Int = 10
}

class TodayViewHolder(val binding: TodaysWeatherListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {
}

