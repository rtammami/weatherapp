package com.reem.weatherapp.home

import com.example.domain.models.Response

data class WeatherState(
    val result: Response = Response(),
    val isLoading: Boolean = false,
    val error: String = ""
)
