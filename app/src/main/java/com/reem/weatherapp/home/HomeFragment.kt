package com.reem.weatherapp.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.reem.weatherapp.databinding.DaysListItemBinding
import com.reem.weatherapp.databinding.HomeFragmentBinding
import com.reem.weatherapp.databinding.TodaysWeatherListItemBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private val viewModel by viewModels<HomeViewModel>()
    private lateinit var binding: HomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = HomeFragmentBinding.inflate(layoutInflater)

        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()

        binding.todaysWeatherRv.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = TodayAdapter("t")
        }
        binding.nextDaysRv.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = DaysAdapter("t")
        }
        return binding.root
    }

    private inner class DaysViewHolder(val binding: DaysListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }

    private inner class DaysAdapter(val temperatures: String) :
        RecyclerView.Adapter<DaysViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DaysViewHolder {
            val binding = DaysListItemBinding.inflate(
                layoutInflater,
                parent,
                false
            )
            return DaysViewHolder(binding)
        }

        override fun onBindViewHolder(holder: DaysViewHolder, position: Int) {
            //
        }

        override fun getItemCount(): Int = 10
    }
}