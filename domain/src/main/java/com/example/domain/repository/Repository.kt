package com.example.domain.repository

import com.example.domain.models.Response

interface Repository {

    suspend fun get30DaysWeather(): Response
}