package com.example.domain.models

data class Temperature(
    val average: Double,
    val average_max: Double,
    val average_min: Double,
    val record_max: Double,
    val record_min: Double
)
