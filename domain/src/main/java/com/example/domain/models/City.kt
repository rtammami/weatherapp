package com.example.domain.models

import com.google.gson.annotations.SerializedName


data class City(
    val id: Int = 0,
    val name: String = "",
    @SerializedName("coord")
    val coordinate: Coordinate = Coordinate(),
    val country: String = ""
)

