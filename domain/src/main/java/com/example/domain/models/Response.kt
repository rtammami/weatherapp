package com.example.domain.models

data class Response(
    val cod: String = "",
    val city: City = City(),
    val message: Double = 0.0,
    val list: List<Weather> = emptyList()
)
