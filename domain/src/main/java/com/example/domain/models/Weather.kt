package com.example.domain.models

data class Weather(
    val dt: Long,
    val humidity: Double,
    val pressure: Double,
    val temp: Temperature,
    val wind_speed: Double
)