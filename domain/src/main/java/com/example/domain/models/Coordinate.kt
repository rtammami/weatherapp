package com.example.domain.models

data class Coordinate(
    val lon: Double = 0.0,
    val lat: Double = 0.0
)
