package com.example.domain.di

import com.example.data.repository.RepositoryImpl
import com.example.data.retrofit.WeatherAPI
import com.example.data.utils.Constants.BASE_URL
import com.example.domain.repository.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun provideWeatherApi(): WeatherAPI {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(WeatherAPI::class.java)
    }

    @Provides
    @Singleton
    fun provideRepository(
        weatherApi: WeatherAPI
    ): Repository{
        return RepositoryImpl(
            weatherApi = weatherApi
        )
    }
}