package com.example.domain.usecase

import com.example.domain.Resource
import com.example.domain.models.Response
import com.example.domain.repository.Repository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class Get30DayWeatherUseCase @Inject constructor(
    private val repo: Repository
) {
    suspend operator fun invoke(): Flow<Resource<Response>> = flow{
        try {
            emit(Resource.Loading())
            val response = repo.get30DaysWeather()
            emit(Resource.Success(response))
        }catch (e: Exception){
            emit(Resource.Error(message = e.message))
        }
    }
}